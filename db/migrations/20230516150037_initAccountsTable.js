const knex = require('knex')

exports.up = function (knex) {
  return knex.schema.createTable('accounts', function (table) {
    table.integer('idUser').unsigned().notNullable()
    table
      .foreign('idUser')
      .references('id')
      .inTable('users')
      .onDelete('Cascade')
      .onUpdate('Cascade')
    table.string('login', 255).notNullable()
    table.string('password', 255).notNullable()
  })
}

exports.down = function (knex) {
  return knex.schema.dropTable('accounts')
}
