const knex = require('knex')

exports.up = function (knex) {
  return knex.schema.createTable('messenger', function (table) {
    table.integer('idGame').unsigned().notNullable()
    table
      .foreign('idGame')
      .references('id')
      .inTable('games')
      .onDelete('Cascade')
      .onUpdate('Cascade')
    table.integer('idUser').unsigned().notNullable()
    table
      .foreign('idUser')
      .references('id')
      .inTable('users')
      .onDelete('Cascade')
      .onUpdate('Cascade')
    table.text('message').notNullable()
    table.timestamp('createdAt', { useTz: false }).notNullable()
  })
}

exports.down = function (knex) {
  return knex.schema.dropTable('messenger')
}
