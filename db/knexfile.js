const password = (module.exports = {
  development: {
    client: 'pg',

    connection: {
      host: '127.0.0.1',
      port: 5433,
      user: 'postgres',
      password: process.env.PG_PASS,
      database: 'db_xoxo_dev',
    },

    migrations: {
      tableName: 'knex_migrations',
    },
  },

  // production: {
  //   client: 'pg',
  //   connection: {
  //     host: '127.0.0.1',
  //     port: 5433,
  //     user: 'postgres',
  //     password: process.env.PG_PASS,
  //     database: 'db_xoxo_home',
  //   },

  //   searchPath: ['knex', 'public'],
  //   migrations: {
  //     tableName: 'knex_migrations',
  //   },
  // },
})
