const express = require('express')
const {
  createField,
  getFieldByIdx,
  updateFieldByIdx,
  deleteFieldByIdx,
} = require('../controllers/gameController')
const router = express.Router()

const { protect } = require('../middleware/authMiddleware')

// router
//   .route('/')
//   .post(protect, createField)
//   .get(protect, getFieldByIdx)
//   .put(protect, updateFieldByIdx)
//   .delete(protect, deleteFieldByIdx)

// ❗ временно без protect
router
  .route('/')
  .post(createField)
  .get(getFieldByIdx)
  .put(updateFieldByIdx)
  .delete(deleteFieldByIdx)

module.exports = router
