const express = require('express')
const usersController = require('../controllers/usersController')
const router = express.Router()

const { protect } = require('../middleware/authMiddleware')
const { registerUser } = require('../modules/authController')

// router.get('/', protect, usersController.getUsers)
// ❗ временно без protect
router.get('/', usersController.getUsers)
// dev:
router.post('/register', registerUser)

module.exports = router
