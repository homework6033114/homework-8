const express = require('express')
const loginController = require('../controllers/loginController')
const authController = require('../modules/authController')

const router = express.Router()

router.get('/', loginController.getLogin)
router.post('/', authController.authorize)

module.exports = router
