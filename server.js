const app = require('./app')
const donenv = require('dotenv').config()
// const debug = require('debug')('XO:server')
const http = require('http')
// const normalizePort = require('normalize-port')

// const port = normalizePort(process.env.PORT)
const port = process.env.PORT || 5000

app.set('port', port)

const server = http.createServer(app)

server.listen(port, () => console.log(`listening on ${port}`))
// server.on('error', onError)
// server.on('listening', onListening)
