const bcrypt = require('bcryptjs')
const fsPromises = require('fs').promises
const fs = require('fs')
const path = require('path')
const accounts = require('../dev-data/accounts.json')

const pathName = path.join(
  path.dirname(require.main.filename),
  'dev-data',
  'accounts.json'
)
const saveData = (data) => {
  fs.writeFile(pathName, JSON.stringify(data), (err) => {
    console.log(err)
  })
}

// Login a new user
const authorize = async (req, res, next) => {
  const { login, password } = req.body

  const data = await fsPromises.readFile('dev-data/userData.json')
  const user = JSON.parse(data)

  // Compare password
  if (user && (await bcrypt.compare(password, user.password))) {
    req.session.isLoggedIn = true
    res.redirect('/users')
  } else {
    res.status(401).render('pages/login', {
      pageTitle: 'Login',
      path: '/login',
      isError: true,
      errorMessage: 'Неверный пароль',
      oldInput: {
        login: login,
        password: password,
      },
    })
  }
}

//dev
// @desc   Register a new user
// @route  /api/user/register
// @access Public
const registerUser = async (req, res) => {
  const { login, password } = req.body

  //Validation
  if (!login || !password) {
    res.status(405)
    throw new Error('Параметры запроса не соответсвуют схеме')
  }

  // res.status(200).json(field)
  // Find if user already exists
  // const userExists = await User.findOne({ where: { email: email } })
  // console.log(userExists)
  // if (userExists) {
  //   res.status(400)
  //   throw new Error('User already exists')
  // }

  // Hash password
  const salt = await bcrypt.genSalt(10)
  const hashedPassword = await bcrypt.hash(password, salt)

  const user = {
    login,
    password: hashedPassword,
  }

  accounts.push(user)
  saveData(accounts)
  res.status(201).json({ message: 'Успешно' })
  // res.status(201).json(user)

  // Create user
  // const user = await User.create({
  //   name,
  //   email,
  //   password: hashedPassword,
  // })

  // if (user) {
  //   res.status(201).json({
  //     id: user.id,
  //     name: user.name,
  //     email: user.email,
  //     token: generateToken(user.id),
  //   })
  // } else {
  //   res.status(400)
  //   throw new Error('Invalid user data')
  // }
}

module.exports = {
  authorize,
  registerUser,
}
