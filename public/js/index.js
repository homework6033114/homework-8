const loginForm = document.getElementById('login-form')
const inputs = document.querySelectorAll('input')
const button = document.querySelector('button')
const login = document.getElementById('login')
const password = document.getElementById('password')

// Запрет на ввод символов, кроме английских букв, цифр и знаков: точка, нижнее подчеркивание:
const inputKeypressHandler = (e) => {
  const key = e.keyCode
  if (
    key != 46 &&
    key != 95 &&
    (key < 48 || key > 57) &&
    (key < 65 || key > 90) &&
    (key < 97 || key > 122)
  ) {
    alert(
      'Допустимо вводить только английские буквы, цифры и знаки: точка, нижнее подчеркивание'
    )
    e.preventDefault()
  }
}

// Запрет на вставку скопированных значений, чтобы предовратить ввод запрещенных символов:
const inputPasteHandler = (e) => {
  alert('Нельзя вставлять скопированные значения в поле')
  e.preventDefault()
}

// Запрет на копирование введенных значений:
const inputCopyHandler = (e) => {
  alert('Нельзя копировать введенные значения  из поля.')
  e.preventDefault()
}
// Активация кнопки Войти, если все поля формы заполнены:
const toggleButtonHandler = () => {
  button.className = `${
    inputs[0].value && inputs[1].value ? 'button active' : 'button'
  }`
}

// Очистка формы от введенных значений
const resetInputValue = (inputArr) => {
  for (input of inputArr) {
    input.value = ''
  }
}

inputs.forEach((item) => {
  item.addEventListener('keypress', inputKeypressHandler)
  item.addEventListener('paste', inputPasteHandler)
  item.addEventListener('copy', inputCopyHandler)
  item.addEventListener('input', toggleButtonHandler)
})

// loginForm.addEventListener('submit', (e) => {
//   e.preventDefault()
//   console.log(login.value, password.value)
//   resetInputValue(inputs)
// })
