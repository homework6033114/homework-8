exports.getLogin = (req, res) => {
  res.render('pages/login', {
    pageTitle: 'Login',
    isError: false,
    errorMessage: '',
    oldInput: {
      login: '',
      password: '',
    },
  })
}
