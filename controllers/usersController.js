const players = require('../dev-data/players')

// @desc   Получение информации о рейтинге игроков
// @route  GET /api/users
// @access Private
const getUsers = (req, res) => {
  // res
  //   .render('pages/Rating', {
  //     pageTitle: 'Rating',
  //     path: '/users',
  //     players: players,
  //   })
  if (!players.length) {
    res.status(404)
    throw new Error('Информация об игроках отсутсвует')
  }
  res.status(200).json(players)
}

module.exports = {
  getUsers,
}
