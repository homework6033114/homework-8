const players = [
  {
    id: 1,
    name: 'Александров Игнат Анатолиевич',
    games: 24534,
    win: 9824,
    lose: 1222,
    winRate: 87,
  },
  {
    id: 2,
    name: 'Шевченко Рафаил Михайлович',
    games: 24534,
    win: 9824,
    lose: 1222,
    winRate: '87',
  },
  {
    id: 3,
    name: 'Мазайло Трофим Артёмович',
    games: 24534,
    win: 9824,
    lose: 1222,
    winRate: '87',
  },
  {
    id: 4,
    name: 'Логинов Остин Данилович',
    games: 24534,
    win: 9824,
    lose: 1222,
    winRate: '87',
  },
  {
    id: 5,
    name: 'Борисов Йошка Васильевич',
    games: 24534,
    win: 9824,
    lose: 1222,
    winRate: '87',
  },
  {
    id: 6,
    name: 'Соловьёв Ждан Михайлович',
    games: 24534,
    win: 9824,
    lose: 1222,
    winRate: '87',
  },
  {
    id: 7,
    name: 'Негода Михаил Эдуардович',
    games: 24534,
    win: 9824,
    lose: 1222,
    winRate: '87',
  },
  {
    id: 8,
    name: 'Гордеев Шамиль Леонидович',
    games: 24534,
    win: 9824,
    lose: 1222,
    winRate: '87',
  },
]
// const players = []

module.exports = players
