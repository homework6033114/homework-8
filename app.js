const express = require('express')
const path = require('path')
const donenv = require('dotenv').config()
const knexConfig = require('./db/knexfile')
const session = require('express-session')
const usersRouter = require('./routes/users')
const loginRouter = require('./routes/login')
const gameRouter = require('./routes/game')

const app = express()

const knex = require('knex')(knexConfig[process.env.NODE_ENV])

app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(express.static(path.join(__dirname, 'public')))

app.use(
  session({
    secret: 'my secret',
    resave: false,
    saveUninitialized: false,
  })
)

app.use('/login', loginRouter)
app.use('/users', usersRouter)
app.use('/game', gameRouter)

module.exports = app
